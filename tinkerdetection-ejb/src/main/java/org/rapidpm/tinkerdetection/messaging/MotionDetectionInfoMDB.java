package org.rapidpm.tinkerdetection.messaging;

import org.rapidpm.tinkerdetection.service.DetectionService;

import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;
import javax.inject.Inject;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import java.util.Date;

/**
 * TinkerDetection - Server
 * A project by RapidPM
 * http://www.rapidpm.org
 * <p>
 * User: Marco Ebbinghaus
 * Date: 04.12.2014
 * Time: 21:37
 */
@MessageDriven(mappedName = "jms/queue/tinkerqueue", activationConfig =
        {
                @ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Queue"),
                @ActivationConfigProperty(propertyName = "destination", propertyValue = "queue/tinkerqueue")
        })
public class MotionDetectionInfoMDB implements MessageListener {

    @Inject
    private DetectionService detectionService;

    @Override
    public void onMessage(final Message message) {
        try{
            final Date date = message.getBody(Date.class);
            System.out.println("MotionDetection received at: " + date);
            detectionService.addDetection(date);
            System.out.println("MotionDetection saved!");
        } catch (final JMSException e) {
            e.printStackTrace();
        }
    }
}
