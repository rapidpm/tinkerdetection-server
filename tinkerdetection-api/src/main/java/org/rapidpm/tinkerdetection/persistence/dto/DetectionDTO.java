package org.rapidpm.tinkerdetection.persistence.dto;

import java.util.Date;

public class DetectionDTO {

    public static final String ID = "id";
    public static final String DATE = "date";
    public static final String DATE_ONLY = "dateOnly";
    public static final String TIME_ONLY = "timeOnly";
    public static final String DETECTION_INFO_DTO = "detectionInfoDTO";
    public static final String PICTURE = DETECTION_INFO_DTO+"."+DetectionInfoDTO.PICTURE_PATH;

    private Long id;

    private Date date;

    private String dateOnly;

    private String timeOnly;

    private DetectionInfoDTO detectionInfoDTO;

    public Long getId() {
        return id;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(final Date date) {
        this.date = date;
    }

    public String getDateOnly() {
        return dateOnly;
    }

    public void setDateOnly(final String dateOnly) {
        this.dateOnly = dateOnly;
    }

    public String getTimeOnly() {
        return timeOnly;
    }

    public void setTimeOnly(final String timeOnly) {
        this.timeOnly = timeOnly;
    }

    public DetectionInfoDTO getDetectionInfoDTO() {
        return detectionInfoDTO;
    }

    public void setDetectionInfoDTO(final DetectionInfoDTO detectionInfoDTO) {
        this.detectionInfoDTO = detectionInfoDTO;
    }
}