package org.rapidpm.tinkerdetection.persistence.dto;

public class DetectionInfoDTO {

    public static final String ID = "id";
    public static final String PICTURE_PATH = "picturePath";
    public static final String DETECTION_DTO = "detectionDTO";

    private Long id;

    private String picturePath;

    private DetectionDTO detectionDTO;

    public Long getId() {
        return id;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    public String getPicturePath() {
        return picturePath;
    }

    public void setPicturePath(final String picturePath) {
        this.picturePath = picturePath;
    }

    public DetectionDTO getDetectionDTO() {
        return detectionDTO;
    }

    public void setDetectionDTO(final DetectionDTO detectionDTO) {
        this.detectionDTO = detectionDTO;
    }
}