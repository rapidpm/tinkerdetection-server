package org.rapidpm.tinkerdetection.service;

import org.rapidpm.tinkerdetection.exception.LoginException;

/**
 * TinkerDetection - Server
 * A project by RapidPM
 * http://www.rapidpm.org
 * <p>
 * User: Marco Ebbinghaus
 * Date: 21.12.2014
 * Time: 14:12
 */
public interface AuthorizationService {

    public void login(final String userName, final String enteredPassword) throws LoginException;

    public Boolean isAuthenticated();

    public void logout();
}
