package org.rapidpm.tinkerdetection.service;

import org.rapidpm.tinkerdetection.persistence.dto.DetectionDTO;

import java.util.Date;
import java.util.List;

/**
 * TinkerDetection - Server
 * A project by RapidPM
 * http://www.rapidpm.org
 * <p>
 * User: Marco Ebbinghaus
 * Date: 07.12.2014
 * Time: 12:16
 */
public interface DetectionService {

    public List<DetectionDTO> getAllDetectionDTOs();
    public List<DetectionDTO> getDetectionDTOsFromTo(final Date from, final Date to);

    public Boolean deleteDetectionByDTO(final DetectionDTO detectionDTO);
    public Boolean deleteDetectionsByDTOs(final List<DetectionDTO> detectionDTOs);
    public Boolean deleteDetectionByID(final Integer detectionDTO);
    public Boolean deleteDetectionByIDs(final List<Integer> detectionDTO);

    public Boolean addDetection(final Date date);
    public Boolean addDetection(final Date date, final String picturePath);
}
