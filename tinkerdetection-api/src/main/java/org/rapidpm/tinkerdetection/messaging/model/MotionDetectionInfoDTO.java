package org.rapidpm.tinkerdetection.messaging.model;

import java.io.Serializable;
import java.util.Date;

/**
 * TinkerDetection - Server
 * A project by RapidPM
 * http://www.rapidpm.org
 * <p>
 * User: Marco Ebbinghaus
 * Date: 04.12.2014
 * Time: 21:35
 */
public class MotionDetectionInfoDTO implements Serializable {

        private Date date;

        public Date getDate() {
            return date;
        }

        public void setDate(final Date date) {
            this.date = date;
        }
}
