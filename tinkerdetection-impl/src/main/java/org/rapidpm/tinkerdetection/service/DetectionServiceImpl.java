package org.rapidpm.tinkerdetection.service;

import org.rapidpm.tinkerdetection.converter.DetectionConverter;
import org.rapidpm.tinkerdetection.persistence.cdi.TinkerdetectionDatabase;
import org.rapidpm.tinkerdetection.persistence.dto.DetectionDTO;
import org.rapidpm.tinkerdetection.persistence.entity.Detection;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * TinkerDetection - Server
 * A project by RapidPM
 * http://www.rapidpm.org
 * <p>
 * User: Marco Ebbinghaus
 * Date: 07.12.2014
 * Time: 12:24
 */
public class DetectionServiceImpl implements DetectionService {

    @Inject
    @TinkerdetectionDatabase
    private EntityManager entityManager;

    @Override
    public List<DetectionDTO> getAllDetectionDTOs() {
        final TypedQuery<Detection> allQuery = entityManager.createNamedQuery(Detection.FIND_ALL, Detection.class);
        final List<Detection> allDetections = allQuery.getResultList();
        final DetectionConverter detectionConverter = new DetectionConverter();
        final List<DetectionDTO> allDetectionDTOs = new ArrayList<>();
        for (final Detection detection : allDetections) {
            allDetectionDTOs.add(detectionConverter.getAsDetectionDTO(detection));
        }
        return allDetectionDTOs;
    }

    @Override
    public List<DetectionDTO> getDetectionDTOsFromTo(final Date from, final Date to) {
        return null;
    }

    @Override
    public Boolean deleteDetectionByDTO(final DetectionDTO detectionDTO) {
        //TODO implementation
        return null;
    }

    @Override
    public Boolean deleteDetectionsByDTOs(final List<DetectionDTO> detectionDTOs) {
        //TODO implementation
        return null;
    }

    @Override
    public Boolean deleteDetectionByID(final Integer detectionDTO) {
        //TODO implementation
        return null;
    }

    @Override
    public Boolean deleteDetectionByIDs(final List<Integer> detectionDTO) {
        //TODO implementation
        return null;
    }

    @Override
    public Boolean addDetection(final Date date) {
        final Detection detection = new Detection();
        detection.setDate(date);
        entityManager.persist(detection);
        entityManager.flush();
        return null;
    }

    @Override
    public Boolean addDetection(final Date date, final String picturePath) {
        //TODO implementation
        return null;
    }
}
