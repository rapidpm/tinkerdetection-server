package org.rapidpm.tinkerdetection.service;

import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.subject.Subject;
import org.rapidpm.tinkerdetection.etc.CryptoUtils;
import org.rapidpm.tinkerdetection.exception.LoginException;
import org.rapidpm.tinkerdetection.persistence.cdi.TinkerdetectionDatabase;
import org.rapidpm.tinkerdetection.persistence.entity.User;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;
import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * TinkerDetection - Server
 * A project by RapidPM
 * http://www.rapidpm.org
 * <p>
 * User: Marco Ebbinghaus
 * Date: 21.12.2014
 * Time: 14:55
 */
public class AuthorizationServiceImpl implements AuthorizationService, Serializable {

    @Inject
    private Subject subject;

    @Inject
    @TinkerdetectionDatabase
    private EntityManager entityManager;

    @Override
    public void login(final String userName, final String enteredPassword) throws LoginException {
        final User user = getUser(userName);
        if (user == null) {
            throw new LoginException();
        }
        final String hashForEnteredPassword = CryptoUtils.generateHash(enteredPassword, user.getSalt());
        final UsernamePasswordToken token = new UsernamePasswordToken(userName, hashForEnteredPassword);
        try {
            subject.login(token);
        } catch (final Exception e) {
            Logger.getAnonymousLogger().log(Level.INFO, e.getMessage());
            throw new LoginException();
        }
    }

    private User getUser(final String userName) {
        final TypedQuery<User> typedQuery = entityManager.createNamedQuery(User.FIND_BY_LOGIN, User.class);
        typedQuery.setParameter("userName", userName);
        try {
            final User user = typedQuery.getSingleResult();
            return user;
        } catch (final NoResultException e) {
            // do nothing, we will check for null later
        }
        return null;
    }

    @Override
    public Boolean isAuthenticated() {
        return subject.isAuthenticated();
    }

    @Override
    public void logout() {
        subject.logout();
    }
}
