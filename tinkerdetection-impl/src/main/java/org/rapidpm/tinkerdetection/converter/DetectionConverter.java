package org.rapidpm.tinkerdetection.converter;

import org.rapidpm.tinkerdetection.persistence.dto.DetectionDTO;
import org.rapidpm.tinkerdetection.persistence.dto.DetectionInfoDTO;
import org.rapidpm.tinkerdetection.persistence.entity.Detection;
import org.rapidpm.tinkerdetection.persistence.entity.DetectionInfo;

import java.time.ZoneId;

/**
 * TinkerDetection - Server
 * A project by RapidPM
 * http://www.rapidpm.org
 * <p>
 * User: Marco Ebbinghaus
 * Date: 07.12.2014
 * Time: 12:27
 */
public class DetectionConverter {

    public Detection getAsDetection(final DetectionDTO detectionDTO){
        final Detection detection = new Detection();
        detection.setId(detectionDTO.getId());
        detection.setDate(detectionDTO.getDate());
        final DetectionInfo detectionInfo = new DetectionInfo();
        detectionInfo.setId(detectionDTO.getDetectionInfoDTO().getId());
        detectionInfo.setPicturePath(detectionDTO.getDetectionInfoDTO().getPicturePath());
        detectionInfo.setDetection(detection);
        detection.setDetectionInfo(detectionInfo);
        return detection;
    }

    public DetectionDTO getAsDetectionDTO(final Detection detection){
        final DetectionDTO detectionDTO = new DetectionDTO();
        detectionDTO.setId(detection.getId());
        detectionDTO.setDate(detection.getDate());
        //TODO use Localdate and LocalTime in DetectionDTO instead of Strings as soon as Vaadin-Grid can handle them
        detectionDTO.setDateOnly(detection.getDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDate().toString());
        detectionDTO.setTimeOnly(detection.getDate().toInstant().atZone(ZoneId.systemDefault()).toLocalTime().toString());
        final DetectionInfoDTO detectionInfoDTO = new DetectionInfoDTO();
        final DetectionInfo detectionInfo = detection.getDetectionInfo();
        if(detectionInfo != null){
            detectionInfoDTO.setId(detectionInfo.getId());
            detectionInfoDTO.setPicturePath(detectionInfo.getPicturePath());
        }
        detectionInfoDTO.setDetectionDTO(detectionDTO);
        detectionDTO.setDetectionInfoDTO(detectionInfoDTO);
        return detectionDTO;
    }
}
