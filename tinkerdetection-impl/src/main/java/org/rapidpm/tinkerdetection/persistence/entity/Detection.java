package org.rapidpm.tinkerdetection.persistence.entity;

import javax.persistence.*;
import java.util.Date;

/**
 * TinkerDetection - Server
 * A project by RapidPM
 * http://www.rapidpm.org
 * <p>
 * User: Marco Ebbinghaus
 * Date: 07.12.2014
 * Time: 12:12
 */

@Entity
@Table(name = "Detections")
@NamedQuery(name = Detection.FIND_ALL, query = "select d from Detection d")
public class Detection {

    public static final String FIND_ALL = "Detections.FIND_ALL";

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "ID", nullable = false)
    private Long id;

    @Column(name = "Date", nullable = false)
    private Date date;

    @OneToOne
    @JoinColumn(name="DetectionInfoID")
    private DetectionInfo detectionInfo;

    public Long getId() {
        return id;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(final Date date) {
        this.date = date;
    }

    public DetectionInfo getDetectionInfo() {
        return detectionInfo;
    }

    public void setDetectionInfo(final DetectionInfo detectionInfo) {
        this.detectionInfo = detectionInfo;
    }
}