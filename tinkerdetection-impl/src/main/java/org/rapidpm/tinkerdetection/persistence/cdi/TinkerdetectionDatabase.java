package org.rapidpm.tinkerdetection.persistence.cdi;

import javax.inject.Qualifier;

/**
 * TinkerDetection - Server
 * A project by RapidPM
 * http://www.rapidpm.org
 * <p>
 * User: Marco Ebbinghaus
 * Date: 07.12.2014
 * Time: 12:01
 */

@Qualifier
public @interface TinkerdetectionDatabase {
}
