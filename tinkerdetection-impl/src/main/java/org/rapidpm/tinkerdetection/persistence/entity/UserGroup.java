package org.rapidpm.tinkerdetection.persistence.entity;

import javax.persistence.*;

/**
 * TinkerDetection - Server
 * A project by RapidPM
 * http://www.rapidpm.org
 * <p>
 * User: Marco Ebbinghaus
 * Date: 07.12.2014
 * Time: 12:12
 */

@Entity
@Table(name = "Usergroups")
@NamedQuery(name = UserGroup.FIND_ALL, query = "select d from UserGroup d")
public class UserGroup {

    public static final String FIND_ALL = "UserGroups.FIND_ALL";

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "ID", nullable = false)
    private Long id;

    @Column(name = "Groupname")
    private String groupName;

    @Column(name = "Description")
    private String description;

    public Long getId() {
        return id;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(final String groupName) {
        this.groupName = groupName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(final String description) {
        this.description = description;
    }
}