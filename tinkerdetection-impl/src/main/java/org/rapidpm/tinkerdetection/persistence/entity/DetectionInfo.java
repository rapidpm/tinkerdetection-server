package org.rapidpm.tinkerdetection.persistence.entity;

import javax.persistence.*;

/**
 * TinkerDetection - Server
 * A project by RapidPM
 * http://www.rapidpm.org
 * <p>
 * User: Marco Ebbinghaus
 * Date: 07.12.2014
 * Time: 12:26
 */

@Entity
@Table(name = "DetectionInfos")
public class DetectionInfo {

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "ID", nullable = false)
    private Long id;

    @Column(name = "PicturePath", nullable = true)
    private String picturePath;

    @OneToOne(mappedBy="detectionInfo")
    private Detection detection;

    public Long getId() {
        return id;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    public String getPicturePath() {
        return picturePath;
    }

    public void setPicturePath(final String picturePath) {
        this.picturePath = picturePath;
    }

    public Detection getDetection() {
        return detection;
    }

    public void setDetection(final Detection detection) {
        this.detection = detection;
    }
}