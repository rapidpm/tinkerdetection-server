package org.rapidpm.tinkerdetection.persistence.cdi.producer;

import com.vaadin.cdi.UIScoped;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;

import javax.enterprise.inject.Produces;

/**
 * TinkerDetection - Server
 * A project by RapidPM
 * http://www.rapidpm.org
 * <p>
 * User: Marco Ebbinghaus
 * Date: 21.12.2014
 * Time: 15:15
 *
 */
public class SubjectFactory {

    @Produces
    @UIScoped
    public Subject getSubject(){
        return SecurityUtils.getSubject();
    }

}
