package org.rapidpm.tinkerdetection.persistence.cdi.producer;

import org.rapidpm.tinkerdetection.persistence.cdi.TinkerdetectionDatabase;

import javax.enterprise.inject.Produces;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * TinkerDetection - Server
 * A project by RapidPM
 * http://www.rapidpm.org
 * <p>
 * User: Marco Ebbinghaus
 * Date: 07.12.2014
 * Time: 12:47
 */

public class DatabaseFactory {
    @Produces
    @TinkerdetectionDatabase
    @PersistenceContext(unitName = "tinkerdetection")
    private EntityManager entityManager;
}