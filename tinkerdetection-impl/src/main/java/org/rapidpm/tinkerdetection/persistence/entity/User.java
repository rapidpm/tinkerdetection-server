package org.rapidpm.tinkerdetection.persistence.entity;

import javax.persistence.*;

/**
 * TinkerDetection - Server
 * A project by RapidPM
 * http://www.rapidpm.org
 * <p>
 * User: Marco Ebbinghaus
 * Date: 07.12.2014
 * Time: 12:12
 */

@Entity
@Table(name = "Users")
@NamedQueries({
        @NamedQuery(name = User.FIND_ALL, query = "select d from User d"),
        @NamedQuery(name = User.FIND_BY_LOGIN, query = "select d from User d where d.userName = :userName")
}
)


public class User {

    public static final String FIND_ALL = "Users.FIND_ALL";
    public static final String FIND_BY_LOGIN = "Users.FIND_BY_LOGIN";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID", nullable = false)
    private Long id;

    @Column(name = "Username")
    private String userName;

    @Column(name = "Email")
    private String email;

    @Column(name = "Password", columnDefinition="char(64)")
    private String password;

    @Column(name = "Salt", columnDefinition="char(64)")
    private String salt;

    @OneToOne
    @JoinColumn(name = "UserGroupID")
    private UserGroup userGroup;

    public Long getId() {
        return id;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(final String userName) {
        this.userName = userName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(final String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(final String password) {
        this.password = password;
    }

    public String getSalt() {
        return salt;
    }

    public void setSalt(final String salt) {
        this.salt = salt;
    }

    public UserGroup getUserGroup() {
        return userGroup;
    }

    public void setUserGroup(final UserGroup userGroup) {
        this.userGroup = userGroup;
    }
}