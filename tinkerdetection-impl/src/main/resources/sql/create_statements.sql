
CREATE TABLE Detections (
         ID BIGINT(20) AUTO_INCREMENT,
         Date DATE UNIQUE,
         DetectionInfoID INT,
         PRIMARY KEY (ID),
         FOREIGN KEY (DetectionInfoID) REFERENCES DetectionInfo(ID),
         INDEX (Date)
     );

CREATE TABLE DetectionInfos (
         ID BIGINT(20) AUTO_INCREMENT,
         PicturePath VARCHAR(255),
         PRIMARY KEY (ID)
     );

CREATE TABLE UserGroups (
        ID BIGINT(20) AUTO_INCREMENT,
        Groupname VARCHAR(255),
        Description VARCHAR(255),
        PRIMARY KEY (ID)
);

CREATE TABLE Users (
        ID BIGINT(20) AUTO_INCREMENT,
        Username VARCHAR(255),
        Email VARCHAR(255),
        Password CHAR(64),
        Salt CHAR(64),
        UserGroupID BIGINT(20),
        PRIMARY KEY (ID),
        FOREIGN KEY (UserGroupID) REFERENCES UserGroups(ID)
);

INSERT INTO UserGroups VALUES (1,'User','normal users, no administrative rights');
INSERT INTO UserGroups VALUES (2,'Moderator','advanced users with moderator rights');
INSERT INTO UserGroups VALUES (3,'Administrator','users with granted access to all features');

INSERT INTO Users VALUES (1,'marco','marco.ebbinghaus@rapidpm.org','ab24a95f44ceca5d2aed4b6d056adddd8539f44c6cd6ca506534e830c82ea8a8','f19b3cc3d49d15c6aa38108e34aba3920c044c157f6e0af4c5f55a7cfda72b00',3);