package org.rapidpm.tinkerdetection.etc;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * TinkerDetection - Server
 * A project by RapidPM
 * http://www.rapidpm.org
 * <p>
 * User: Marco Ebbinghaus
 * Date: 11.01.2015
 * Time: 12:51
 */
public class CryptoUtilsTest {

    @Test
    public void generateSaltTest(){
        final String generatedSalt = CryptoUtils.generateSalt();
        assertTrue("Generated Salt has wrong length.", generatedSalt.length() == CryptoUtils.SALT_BYTE_SIZE * 2);
    }

    @Test
    public void generateHashTest(){
        final String enteredPassword = "marco";
        final String salt = "dc35c955144a7c955ed6d7f9981b09b85da16817c2f4c29aefd465f29af239ee";
        final String expectedHash = "ab24a95f44ceca5d2aed4b6d056adddd8539f44c6cd6ca506534e830c82ea8a8";
        final String generatedHash = CryptoUtils.generateHash(enteredPassword, salt);
        assertEquals("Generated hash didn't match expected hash.", expectedHash, generatedHash);
    }
}
