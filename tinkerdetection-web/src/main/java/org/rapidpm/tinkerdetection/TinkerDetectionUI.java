package org.rapidpm.tinkerdetection;

import com.vaadin.annotations.Theme;
import com.vaadin.cdi.CDIUI;
import com.vaadin.cdi.CDIViewProvider;
import com.vaadin.navigator.Navigator;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.server.VaadinRequest;
import com.vaadin.ui.UI;
import org.rapidpm.tinkerdetection.etc.Constants;
import org.rapidpm.tinkerdetection.etc.Language;
import org.rapidpm.tinkerdetection.service.AuthorizationService;
import org.rapidpm.tinkerdetection.ui.views.ErrorView;

import javax.inject.Inject;
import java.util.Locale;
import java.util.ResourceBundle;

@Theme("darkvalo")
@CDIUI("")
@SuppressWarnings("serial")
public class TinkerDetectionUI extends UI implements ViewChangeListener {

    @Inject
    private CDIViewProvider viewProvider;

    @Inject
    private AuthorizationService authorizationService;

    private ResourceBundle messages;


    @Override
    protected void init(VaadinRequest request) {
        setLocale(getLocale());
        final Navigator navigator = new Navigator(this, this);
        navigator.addProvider(viewProvider);
        if (authorizationService.isAuthenticated()) {
            navigator.navigateTo(Constants.MAIN);
        } else {
            navigator.navigateTo(Constants.LOGIN);
        }
        navigator.addViewChangeListener(this);
        navigator.setErrorView(ErrorView.class);
    }

    @Override
    public boolean beforeViewChange(ViewChangeEvent event) {

        //If user is logged in, redirect from root to main-View
        if (authorizationService.isAuthenticated() && event.getViewName().equals("")) {
            event.getNavigator().navigateTo(Constants.MAIN);
            return false;
        }

        //If user is logged in, redirect from login to main-View
        if (authorizationService.isAuthenticated() && event.getViewName().equals(Constants.LOGIN)) {
            event.getNavigator().navigateTo(Constants.MAIN);
            return false;
        }

        //If user is not logged in, redirect from every view to login (but not from login to login)
        if (!authorizationService.isAuthenticated() && !event.getViewName().equals(Constants.LOGIN)) {
            event.getNavigator().navigateTo(Constants.LOGIN);
            return false;
        }
        return true;
    }

    @Override
    public void afterViewChange(ViewChangeEvent event) {
        // TODO Auto-generated method stub
    }

    public ResourceBundle getMessages() {
        return messages;
    }

    public void setLocale(final Language language){
        final Locale locale = new Locale(language.getLocaleIsoCode());
        setLocale(locale);
    }

    @Override
    public void setLocale(final Locale locale) {
        super.setLocale(locale);
        messages = ResourceBundle.getBundle("MessagesBundle", getLocale());
        Language.setResourceBundle(messages);
    }
}
