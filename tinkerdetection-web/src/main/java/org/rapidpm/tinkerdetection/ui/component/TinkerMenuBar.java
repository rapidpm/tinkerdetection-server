package org.rapidpm.tinkerdetection.ui.component;

import com.vaadin.ui.MenuBar;
import org.rapidpm.tinkerdetection.etc.Constants;
import org.rapidpm.tinkerdetection.service.AuthorizationService;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

/**
 * TinkerDetection - Server
 * A project by RapidPM
 * http://www.rapidpm.org
 * <p>
 * User: Marco Ebbinghaus
 * Date: 28.12.2014
 * Time: 15:59
 */
public class TinkerMenuBar extends MenuBar {

    @Inject
    private AuthorizationService authorizationService;

    @PostConstruct
    public void init(){
        addItem("Logout", menuItem -> {
            authorizationService.logout();
            getSession().close();
            getUI().getPage().setLocation(Constants.ROOT_CONTEXT);
        });
    }
}
