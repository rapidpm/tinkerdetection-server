package org.rapidpm.tinkerdetection.ui.views.template;

/**
 * TinkerDetection - Server
 * A project by RapidPM
 * http://www.rapidpm.org
 * <p>
 * User: Marco Ebbinghaus
 * Date: 28.12.2014
 * Time: 00:14
 */
public interface Screen {
    public void buildView();
    public void doLayout();
}
