package org.rapidpm.tinkerdetection.ui.views;

import com.vaadin.cdi.CDIView;
import com.vaadin.data.util.IndexedContainer;
import com.vaadin.event.ShortcutAction;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.server.ThemeResource;
import com.vaadin.ui.*;
import org.rapidpm.tinkerdetection.TinkerDetectionUI;
import org.rapidpm.tinkerdetection.etc.Constants;
import org.rapidpm.tinkerdetection.etc.Language;
import org.rapidpm.tinkerdetection.exception.LoginException;
import org.rapidpm.tinkerdetection.service.AuthorizationService;
import org.rapidpm.tinkerdetection.ui.views.template.Screen;

import javax.inject.Inject;
import java.util.Locale;
import java.util.ResourceBundle;

/**
 * TinkerDetection - Server
 * A project by RapidPM
 * http://www.rapidpm.org
 * <p>
 * User: Marco Ebbinghaus
 * Date: 14.12.2014
 * Time: 10:51
 */
@CDIView(Constants.LOGIN)
public class LoginView extends CustomComponent implements View, Screen {

    @Inject
    protected AuthorizationService authorizationService;

    private ResourceBundle messages;
    
    private VerticalLayout frameLayout;
    private VerticalLayout loginLayout;
    private Panel loginPanel;
    private TextField usernameField;
    private PasswordField pwField;
    private ComboBox languageBox;
    private Button loginButton;
    private Image image;

    @Override
    public void enter(final ViewChangeListener.ViewChangeEvent event) {
        buildView();
        doLayout();
    }

    @Override
    public void buildView() {
        messages = ((TinkerDetectionUI) getUI()).getMessages();
        frameLayout = new VerticalLayout();
        loginLayout = new VerticalLayout();
        loginPanel = new Panel();
        usernameField = new TextField(messages.getString("username"));
        pwField = new PasswordField(messages.getString("password"));
        loginButton = new Button(messages.getString("login"));
        image = new Image(null, new ThemeResource("img/800px-Tinkerforge_logo.svg.png"));
        usernameField.setId("username");
        pwField.setId("password");
        final IndexedContainer container = new IndexedContainer();
        for (final Language language : Language.values()) {
            container.addItem(language);
        }
        languageBox = new ComboBox(messages.getString("language"));
        languageBox.setContainerDataSource(container);
        languageBox.setItemCaptionMode(AbstractSelect.ItemCaptionMode.ID);
        languageBox.setNewItemsAllowed(false);
        languageBox.setTextInputAllowed(false);
        languageBox.setNullSelectionAllowed(false);
        final Locale locale = getUI().getLocale();
        switch (locale.toString().toLowerCase()) {
            case "de_de":
                languageBox.setValue(Language.GERMAN);
                break;
            case "en_us":
                languageBox.setValue(Language.ENGLISH);
                break;
            default:
                languageBox.setValue(Language.ENGLISH);
                break;

        }
        loginButton.addClickListener(clickEvent -> {
            try {
                authorizationService.login(usernameField.getValue(), pwField.getValue());
                ((TinkerDetectionUI) getUI()).setLocale((Language) languageBox.getValue());
                if (authorizationService.isAuthenticated()) {
                    getUI().getNavigator().navigateTo(Constants.MAIN);
                    //VaadinService.reinitializeSession(VaadinService.getCurrentRequest());
                } else {
                    getUI().getNavigator().navigateTo(Constants.LOGIN);
                }
            } catch (final LoginException e) {
                getUI().getNavigator().navigateTo(Constants.LOGIN);
                Notification.show(messages.getString("messages.login.error"));
            }


        });
        loginButton.setClickShortcut(ShortcutAction.KeyCode.ENTER);
        loginLayout.addComponents(usernameField, pwField, languageBox, loginButton);
        loginPanel.setContent(loginLayout);
        frameLayout.addComponents(image, loginPanel);
        setCompositionRoot(frameLayout);
    }

    @Override
    public void doLayout() {
        usernameField.setSizeFull();
        pwField.setSizeFull();
        languageBox.setSizeFull();
        loginButton.setWidth("50%");
        loginLayout.setSizeFull();
        loginLayout.setSpacing(true);
        loginLayout.setMargin(true);
        loginPanel.setWidth("330px");
        image.setSizeUndefined();
        frameLayout.setSizeFull();
        frameLayout.setMargin(false);
        frameLayout.setComponentAlignment(image, Alignment.BOTTOM_CENTER);
        frameLayout.setComponentAlignment(loginPanel, Alignment.TOP_CENTER);
        setSizeFull();
    }
}
