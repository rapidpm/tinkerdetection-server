package org.rapidpm.tinkerdetection.ui.views;

import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.ui.Label;
import com.vaadin.ui.VerticalLayout;

/**
 * TinkerDetection - Server
 * A project by RapidPM
 * http://www.rapidpm.org
 * <p>
 * User: Marco Ebbinghaus
 * Date: 14.12.2014
 * Time: 10:51
 */
public class ErrorView extends VerticalLayout implements View {
    @Override
    public void enter(final ViewChangeListener.ViewChangeEvent event) {
        addComponent(new Label("Error!"));
    }
}
