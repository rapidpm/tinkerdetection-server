package org.rapidpm.tinkerdetection.ui.views.template;

import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.VerticalLayout;
import org.rapidpm.tinkerdetection.TinkerDetectionUI;
import org.rapidpm.tinkerdetection.service.AuthorizationService;
import org.rapidpm.tinkerdetection.ui.component.TinkerMenuBar;

import javax.inject.Inject;
import java.util.ResourceBundle;

/**
 * TinkerDetection - Server
 * A project by RapidPM
 * http://www.rapidpm.org
 * <p>
 * User: Marco Ebbinghaus
 * Date: 27.12.2014
 * Time: 23:57
 */
public class ViewTemplate extends CustomComponent implements View {

    @Inject
    protected AuthorizationService authorizationService;

    @Inject
    protected TinkerMenuBar menuBar;

    protected VerticalLayout frameLayout;

    protected ResourceBundle messages;


    @Override
    public void enter(final ViewChangeListener.ViewChangeEvent event) {
        messages = ((TinkerDetectionUI)getUI()).getMessages();
        buildViewFrame();
        doFrameLayout();
    }

    public void buildViewFrame() {

        frameLayout = new VerticalLayout();
        frameLayout.addComponent(menuBar);
        setCompositionRoot(frameLayout);
    }

    public void doFrameLayout() {
        menuBar.setWidth("100%");
        frameLayout.setSizeFull();
        setSizeFull();
    }
}
