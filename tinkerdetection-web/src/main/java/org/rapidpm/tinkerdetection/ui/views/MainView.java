package org.rapidpm.tinkerdetection.ui.views;

import com.vaadin.cdi.CDIView;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.ui.Grid;
import com.vaadin.ui.Label;
import org.rapidpm.tinkerdetection.etc.Constants;
import org.rapidpm.tinkerdetection.persistence.dto.DetectionDTO;
import org.rapidpm.tinkerdetection.service.DetectionService;
import org.rapidpm.tinkerdetection.ui.component.detection.DetectionGrid;
import org.rapidpm.tinkerdetection.ui.views.template.Screen;
import org.rapidpm.tinkerdetection.ui.views.template.ViewTemplate;

import javax.inject.Inject;
import java.util.List;

/**
 * TinkerDetection - Server
 * A project by RapidPM
 * http://www.rapidpm.org
 * <p>
 * User: Marco Ebbinghaus
 * Date: 14.12.2014
 * Time: 10:51
 */
@CDIView(Constants.MAIN)
public class MainView extends ViewTemplate implements Screen {

    @Inject
    DetectionService detectionService;

    private Label contentLabel;
    private Grid detectionGrid;

    @Override
    public void enter(final ViewChangeListener.ViewChangeEvent event) {
        super.enter(event);
        buildView();
        doLayout();
    }

    @Override
    public void buildView() {
        contentLabel = new Label("Content");
        final List<DetectionDTO> detectionDTOs = detectionService.getAllDetectionDTOs();
        detectionGrid = new DetectionGrid(detectionDTOs);
        frameLayout.addComponent(contentLabel);
        frameLayout.addComponent(detectionGrid);
    }

    @Override
    public void doLayout() {
        frameLayout.setExpandRatio(detectionGrid, 1.0f);
    }
}
