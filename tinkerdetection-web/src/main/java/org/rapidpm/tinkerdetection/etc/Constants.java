package org.rapidpm.tinkerdetection.etc;

/**
 * TinkerDetection - Server
 * A project by RapidPM
 * http://www.rapidpm.org
 * <p>
 * User: Marco Ebbinghaus
 * Date: 27.12.2014
 * Time: 23:54
 */
public class Constants {


    public static final String ROOT_CONTEXT = "/tinkerdetection-web";

    // Views
    public static final String MAIN = "tinkerdetection";
    public static final String LOGIN = "login";
    public static final String ERROR = "404";
}
