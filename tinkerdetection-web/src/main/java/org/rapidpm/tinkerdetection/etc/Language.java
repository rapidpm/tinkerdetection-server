package org.rapidpm.tinkerdetection.etc;

import java.util.ResourceBundle;

/**
 * TinkerDetection - Server
 * A project by RapidPM
 * http://www.rapidpm.org
 * <p>
 * User: Marco Ebbinghaus
 * Date: 28.12.2014
 * Time: 16:41
 */
public enum Language {
    ENGLISH("en_us"),
    GERMAN("de_de");

    private static ResourceBundle messages;
    private String localeIsoCode;

    private Language(final String localeIsoCode) {
        this.localeIsoCode = localeIsoCode;
    }

    public static void setResourceBundle(final ResourceBundle messages) {
        Language.messages = messages;
    }

    public String toString() {
        return messages.getString(name());
    }

    public String getLocaleIsoCode() {
        return localeIsoCode;
    }
}
